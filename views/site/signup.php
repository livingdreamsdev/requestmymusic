<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
?>
<div class="user-signup">
    
    <h2>Sign Up </h2>
    
    <?php $form = ActiveForm::begin(); ?> 
    
    <div class="row">
        <?= $form->field($model, 'name', ['options' => ['class' => 'col-md-6']]) ?>
        <?php //$form->field($model, 'username') ?>
        <?= $form->field($model, 'email', ['options' => ['class' => 'col-md-6']]) ?>
        <?= $form->field($model, 'password', ['options' => ['class' => 'col-md-6']])->passwordInput() ?>
        <?= $form->field($model, 'confirmPassword', ['options' => ['class' => 'col-md-6']])->passwordInput() ?>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div><!-- user-signup -->
