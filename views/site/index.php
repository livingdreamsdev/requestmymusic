<?php
/* @var $this yii\web\View */

$this->title = 'Request My Music';
?>
<div class="site-index">
    <div class="body-content">
        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="jumbotron">
                <?php if (Yii::$app->user->identity->role == 0): ?>
                    <h1>Request Lists: </h1>
                    <ul class="list-group">
                        <?php foreach ($events as $event): ?>
                            <li><?= yii\helpers\Html::a($event->name, ['event/view/', 'id' => $event->id]); ?></li>
                        <?php endforeach; ?>
                    </ul>
                    <a href="<?= yii\helpers\Url::to(['event/create']) ?>" class="btn btn-success">Create a list</a>
                <?php endif ?>
            </div>
        <?php else: ?>
            <div class="jumbotron">
                <h1 class="cover-heading">Welcome to Request Music</h1>
                <p class="lead">Hassle-free requesting delivered to you. Live requesting optimized for viewing on-the-go. Guests quickly and easily submit requests. No downloading additional apps. Sign up for our invite only alpha test.</p>
            </div>
            <div class="authform">
                <?=
                $this->render('_form', [
                    'model' => $model,
                ])
                ?>
            </div>
        <?php endif ?>
    </div>
</div>
