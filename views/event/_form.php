<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */
$slug = $model->guestLink == null ? "&lt;slug&gt;" : $model->guestLink;
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'guestLink')->textInput(['maxlength' => true]) ?>
    <p> Guests can submit requests to this URL based on above,
        <label>IE: <a href="#"><?= yii\helpers\Url::to('events/' . $slug, true) ?></a></label></p>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
