<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Event', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="events-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'guestLink',
            ['attribute' => 'RequestURL', 'format' => 'raw', 'value' => function($model) {
                    $link = yii\helpers\Url::to(['event/' . $model->guestLink], true);
                    return Html::a($link, $link);
                }]
        ],
    ])
    ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <h2>Song Requests:</h2>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'tableOptions' => ['class' => 'table table-striped'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id',
            'song.name',
            //'userId',
            //'eventId',
            ['attribute' => 'Preview','format' => 'raw', 'value' => function($model) {
                    return Html::a('Listen', $model->song->previewLink);
                }],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

    <div class="">
        <?=
        $this->render('//song/_form', [
            'model' => $song,
        ])
        ?>
    </div>
</div>
