<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SongSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Songs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="song-index">

    <h1><?= Html::encode($this->title) ?> <?= Html::a('Add Song', ['create'], ['class' => 'pull-right btn btn-success']) ?></h1>

    <p>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            ['attribute' => 'userId', 'value' => 'user.name'],
            'name',
            ['attribute' => 'previewLink', 'value' => function($model) {
                    return Html::a($model->previewLink, $model->previewLink, ['target' => 'blank']);
                }, 'format' => 'raw'],
            ['attribute' => 'trackViewLink', 'value' => function($model) {
                    return Html::a($model->trackViewLink, $model->trackViewLink, ['target' => 'blank']);
                }, 'format' => 'raw']
            ,
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>


</div>
