<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Song */

$this->title = 'Add Song';
$this->params['breadcrumbs'][] = ['label' => 'Songs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="song-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (!empty($tracks)): ?>
        <?php foreach ($tracks as $track): if($track['preview_url']): ?>
            <a href="<?= $track['preview_url'] ?>" target="_blank"><h4><?= $track['name'] ?></h4></a>
            <?php //var_dump($track) ?>
        <?php endif; endforeach; ?>
    <?php endif; ?>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
