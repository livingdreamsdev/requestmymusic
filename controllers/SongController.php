<?php

namespace app\controllers;

use Yii;
use app\models\Song;
use app\models\SongSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SongController implements the CRUD actions for Song model.
 */
class SongController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Song models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SongSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Song model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Song model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Song();
        $tracks = [];
        if ($model->load(Yii::$app->request->post())) {
            $spotify = \Yii::$app->authClientCollection->getClient('spotify');
            $accessToken = $spotify->authenticateClient();
            //var_dump($accessToken);
            $response = $spotify->createApiRequest()
                    ->setMethod('GET')
                    ->setUrl('search')
                    ->setData(['q' => $model->name, 'type' => 'track'])
                    ->send();
            //has no errors
            if ($response->isOK) {
                //var_dump($response->data['tracks']);
                if ($response->data['tracks']['total'] == 0)
                    \Yii::$app->session->setFlash('info', 'No track found');
                foreach ($response->data['tracks']['items'] as $track) {
                    if ($track['preview_url']) {
                        //var_dump($track['href']);
                        $model->userId = Yii::$app->user->isGuest ? 0 : Yii::$app->user->id;
                        $model->name = $track['name'];
                        $model->previewLink = $track['preview_url'];
                        $model->trackViewLink = $track['external_urls']['spotify'];
                        $model->save();
                        //var_dump($model->getErrors());
                        break;
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                    //var_dump($response->statusCode);
                }
                $tracks = $response->data['tracks']['items'];
            } elseif (isset($response->data['error']))
                \Yii::$app->session->setFlash('error', $response->data['error']['message']);
        }

        return $this->render('create', [
                    'model' => $model,
                    'tracks' => $tracks
        ]);
    }

    /**
     * Updates an existing Song model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Song model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Song model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Song the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Song::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
