<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $confirmPassword
 * @property string $name
 * @property int $roles
 * @property int $status
 * @property string $accessToken
 * @property string $authKey
 *
 * @property Request[] $requests
 * @property Song[] $songs
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

    public $confirmPassword;
    public static $roles = ['Super Admin', 'User'];

    const SuperAdmin = 0;
    const User = 1;

    public static $status = ['Deactive', 'Active'];
    public $username;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['email', 'password', 'name', 'status', 'accessToken', 'authKey'], 'required', 'except' => 'update'],
            [['status', 'role'], 'integer'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 128],
            [['password'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 64],
            [['accessToken', 'authKey'], 'string', 'max' => 255],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Password dont match'],
            [['password'], 'string', 'min' => 6],
            [['password'], 'match', 'pattern' => '/\d/', 'message' => 'Password should contain at least one number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            //'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'name' => 'Name',
            'role' => 'Role',
            'status' => 'Status',
            'accessToken' => 'Access Token',
            'authKey' => 'Auth Key',
        ];
    }

    /**
     * Gets query for [[Requests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequests() {
        return $this->hasMany(Request::className(), ['userId' => 'id']);
    }

    /**
     * Gets query for [[Songs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSongs() {
        return $this->hasMany(Song::className(), ['userId' => 'id']);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    public static function findByEmail($email) {
        return static::findOne(['email' => $email]);
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        //return $this->password === $password;
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->name;
    }

    public function getRole() {
        return self::$roles[$this->role];
    }

    public function getStatus() {
        return self::$status[$this->status];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['accessToken' => $token]);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * Before validate
     */
    public function beforeValidate() {
        $name = explode('@', $this->email);
        $this->username = $name[0];
        if ($this->authKey == null)
            $this->authKey = \Yii::$app->security->generateRandomString();
        if ($this->accessToken == null)
            $this->accessToken = \Yii::$app->security->generateRandomString(64);

        return parent::beforeValidate();
    }

    /**
     * @param type $insert
     */
    public function beforeSave($insert) {
        //hash password
        if (isset($this->password))
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        //iff saved
        if (parent::beforeSave($insert)) {
            return true;
        }
        return false;
    }

}
