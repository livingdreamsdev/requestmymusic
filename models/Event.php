<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property int $userId
 * @property string $name
 * @property string $guestLink
 *
 * @property Request[] $requests
 */
class Event extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'guestLink', 'userId'], 'required'],
            [['userId'], 'integer'],
            [['guestLink'], 'unique'],
            [['name', 'guestLink'], 'string', 'max' => 255],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'userId' => 'Owner',
            'name' => 'Name',
            'guestLink' => 'Custom URL slug',
        ];
    }

    /**
     * Gets query for [[Requests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequests() {
        return $this->hasMany(Request::className(), ['eventId' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

}
